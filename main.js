import * as tfjsNode from '@tensorflow/tfjs-node';
import * as mobilenet from '@tensorflow-models/mobilenet';
import snoowrap from "snoowrap";
import ImgurAnonymousUploader from 'imgur-anonymous-uploader';
import {promises} from "fs";
import dotenv from 'dotenv';
const {readdir, readFile} = promises;

const config = {
  imageFolder: '../../virtualExperiences',  //omit the ending slash
  interval: 1000*60*31.7,   //31.7 minutes
  mobilenetVersion: 1     //the results were better with version 1 than 2
};

dotenv.config();

let randomElement = array => array[Math.floor(Math.random()*(array.length))];

const conjunctions = [" and the ", " with the ", " near the ", " in the vicinity of the ", " close to the ", " within reach of the ", " next to the ", " alongside the ", " "];

const r = new snoowrap({
  userAgent: process.env.REDDIT_USER_AGENT,
  clientId: process.env.REDDIT_CLIENT_ID,
  clientSecret: process.env.REDDIT_CLIENT_SECRET,
  username: process.env.REDDIT_USERNAME,
  password: process.env.REDDIT_PASWORD
});

(async () => {
  const model = await mobilenet.load({version:config.mobilenetVersion,alpha:1});

  let cron = async () => {

    let imagePath = await readdir(config.imageFolder).then((dirs) => `${config.imageFolder}/${randomElement(dirs)}`);
    console.log(imagePath);
    const image = await readFile(imagePath);
    const decodedImage = tfjsNode.node.decodeImage(image, 3);
    const uploader = new ImgurAnonymousUploader(process.env.IMGUR_ID);
    const uploadResponse = await uploader.upload(imagePath);
    const url = uploadResponse.url;
    console.log(url);
    let classes = (await model.classify(decodedImage));
    let pickedClasses = classes.filter(x=>x.probability>0.2)

    let titleWords = (pickedClasses.length===0?([randomElement(classes)]):pickedClasses)
      .filter(x=>x)
      .map(x=>randomElement(x.className.split(", ")))
    let title = titleWords.length>0?titleWords.reduce((a,b) => a+randomElement(conjunctions)+b):"";
    title = title.charAt(0).toUpperCase() + title.slice(1)
    console.log(`posting link to ${url} with title: "${title}"`);
    let response = await r.getSubreddit('imagesLackingContext').submitLink({url,title});
    response.reply("Originally posted on zucced facebook page Ｖｉｒｔｕａｌ Ｅｘｐｅｒｉｅｎｃｅｓ イキがいい , superseded by Ｖｉｒｔｕａｌ Ｅｘｐｅｒｉｅｎｃｅｓ ２ イキがいい — https://www.facebook.com/VirtualExperiences2");
    console.log(response);
  }
  await cron();
  setInterval(cron, config.interval)

})()



/*
r.getSubreddit('AskReddit').getWikiPage('bestof').content_md.then(console.log);*/
