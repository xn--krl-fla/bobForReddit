import express from "express";
const app = express()
const port = 3000
import snoowrap from "snoowrap";
import {promises} from "fs";
import dotenv from 'dotenv';
const {readdir, readFile} = promises;
import seedrandom from "seedrandom";


const config = {online: false};

dotenv.config();

const r = new snoowrap({
  userAgent: process.env.REDDIT_USER_AGENT,
  clientId: process.env.REDDIT_CLIENT_ID,
  clientSecret: process.env.REDDIT_CLIENT_SECRET,
  username: process.env.REDDIT_USERNAME,
  password: process.env.REDDIT_PASWORD
});
let data;

if(config.online) {
  data = await r.getSubreddit("explainlikeimfive").getTop({time: "year", limit: 100});
  await promises.writeFile("koberec.json", JSON.stringify(data),"utf-8");
}
else {
  data = await promises.readFile("koberec.json", 'utf8').then(x=>JSON.parse(x));
}
let interrogators = ["who","what","which","whose","when","where","how","why"];

let eli5remover = (t) => t.replace(/(ELI5: )|(ELI5 )/gi,"");



let filtered = data.filter(x=>interrogators.map(i=>eli5remover(x.title).toLowerCase().startsWith(i)).reduce((a,b) => a||b)).filter(x=>x.title.length<160);


let spanner = t => t.split("").map(x=>`<span>${x}</span>`).join("");

app.get('/', (req, res) => {

  var rng = seedrandom(Math.floor(Date.now()/1000/60));
  let post = filtered[Math.floor(rng() * filtered.length)];
  let text = spanner(eli5remover(post.title));


  res.send(`
  <html lang="en">
  <head>
 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Cutive+Mono&display=swap" rel="stylesheet"> 
<style>
*{
font-family: 'Cutive Mono', monospace;
font-size: 36px;
background-color: black;
color:white;
}
span{
visibility: hidden;
}
</style>
<title>the question</title></head>
<body>
<div style="display: flex;justify-content: center;align-items: center;bottom: 0;top:0;left:0;right:0;position: fixed">
<div id="text" style="position: relative;top:-1vh;padding: 10vw; text-align: center">${text}</div>
</div></body>
<script>
Array.from(document.querySelector("#text").children).forEach((x,i) => setTimeout(()=>x.style.visibility="initial",1000+i*100))
console.log(JSON.parse(\`${JSON.stringify(post)}\`))
</script>
  </html>
  `)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
